# The Broken Component Repo
This repo contains a next.js (react) app with two broken components to test a developers ability to find bugs by eye.

## How to run
To install deps:
`yarn install --frozen-lockfile`
And to run use:
`yarn dev`
