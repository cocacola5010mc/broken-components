import React from 'react'

export default function counter({ onUpdate = () => {} }) {
  let value = 0

  return (
    <div>
      Value: {value}
      <button
        onClick={() => {
          ++value
        }}
      >
        +
      </button>
      <button
        onClick={() => {
          --value
        }}
      >
        -
      </button>
    </div>
  )
}
