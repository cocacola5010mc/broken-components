import React, { useState } from 'react'

export default function taskEntryBox() {
  const [tasks, setTasks] = useState(null)
  const [enteredTask, setEnteredTask] = useState(null)

  function addTask() {
    setTasks([...tasks, enteredTask])
    setEnteredTask('')
  }

  return (
    <div>
      <input
        type="text"
        value={enteredTask}
        onKeyPress={(e) => {
          if (e.code === 'Comma') {
            addTask()
          }
        }}
        onChange={(e) => {
          setEnteredTask(e.target.value)
        }}
      />
      <button onClick={() => addTask()}>Add: {enteredTask}</button>
      <ul>
        {tasks.map((task) => (
          <li key={task}>{task}</li>
        ))}
      </ul>
    </div>
  )
}
