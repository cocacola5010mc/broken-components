import Head from 'next/head'
import styles from '../styles/Home.module.css'

import TaskEntryBox from '../components/taskEntryBox.js'
import Counter from '../components/counter.js'

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Broken Component Challange</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>Goal: Find Errors By Inspection</h1>
        <h2>Task 1: Counter Component</h2>
        <p className={styles.description}>
          This component has been desigened to increment and decrement on the click of a button.
        </p>
        <Counter />
        <h2>Task 2: Text Entry Component</h2>
        <p className={styles.description}>
          The task entry component has been updated to add items when comma "," is pressed. code.
        </p>
        <TaskEntryBox />
      </main>
    </div>
  )
}
